# Spring Boot Jasypt Sample

Encryption/Decryption of properties in Spring boot with Jasypt

#### Things to do to run locally:

Clone this project :

```
git clone https://gitlab.com/hendisantika/spring-boot-jasypt-sample.git
```

Go to the folder :
```
cd spring-boot-jasypt-sample
```

Run the app :
```
gradle clean bootRun
```

Result:
```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.3.RELEASE)

2019-02-21 13:29:08.832  INFO 46670 --- [           main] c.h.j.JasyptSampleApplication            : Starting JasyptSampleApplication on e744-r5200726.azlife.allianz.co.id with PID 46670 (/Users/hendisantika/IdeaProjects/jasypt-sample/out/production/classes started by hendisantika in /Users/hendisantika/IdeaProjects/jasypt-sample)
2019-02-21 13:29:08.834  INFO 46670 --- [           main] c.h.j.JasyptSampleApplication            : No active profile set, falling back to default profiles: default
2019-02-21 13:29:09.212  INFO 46670 --- [           main] ptablePropertiesBeanFactoryPostProcessor : Post-processing PropertySource instances
2019-02-21 13:29:09.275  INFO 46670 --- [           main] c.u.j.EncryptablePropertySourceConverter : Converting PropertySource configurationProperties [org.springframework.boot.context.properties.source.ConfigurationPropertySourcesPropertySource] to AOP Proxy
2019-02-21 13:29:09.277  INFO 46670 --- [           main] c.u.j.EncryptablePropertySourceConverter : Converting PropertySource systemProperties [org.springframework.core.env.MapPropertySource] to EncryptableMapPropertySourceWrapper
2019-02-21 13:29:09.277  INFO 46670 --- [           main] c.u.j.EncryptablePropertySourceConverter : Converting PropertySource systemEnvironment [org.springframework.boot.env.SystemEnvironmentPropertySourceEnvironmentPostProcessor$OriginAwareSystemEnvironmentPropertySource] to EncryptableMapPropertySourceWrapper
2019-02-21 13:29:09.277  INFO 46670 --- [           main] c.u.j.EncryptablePropertySourceConverter : Converting PropertySource random [org.springframework.boot.env.RandomValuePropertySource] to EncryptablePropertySourceWrapper
2019-02-21 13:29:09.277  INFO 46670 --- [           main] c.u.j.EncryptablePropertySourceConverter : Converting PropertySource applicationConfig: [classpath:/application.properties] [org.springframework.boot.env.OriginTrackedMapPropertySource] to EncryptableMapPropertySourceWrapper
2019-02-21 13:29:09.300  INFO 46670 --- [           main] c.u.j.filter.DefaultLazyPropertyFilter   : Property Filter custom Bean not found with name 'encryptablePropertyFilter'. Initializing Default Property Filter
2019-02-21 13:29:09.303  INFO 46670 --- [           main] c.u.j.r.DefaultLazyPropertyResolver      : Property Resolver custom Bean not found with name 'encryptablePropertyResolver'. Initializing Default Property Resolver
2019-02-21 13:29:09.304  INFO 46670 --- [           main] c.u.j.d.DefaultLazyPropertyDetector      : Property Detector custom Bean not found with name 'encryptablePropertyDetector'. Initializing Default Property Detector
2019-02-21 13:29:09.305  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : String Encryptor custom Bean not found with name 'jasyptStringEncryptor'. Initializing Default String Encryptor
2019-02-21 13:29:09.310  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.algorithm, using default value: PBEWithMD5AndDES
2019-02-21 13:29:09.311  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.keyObtentionIterations, using default value: 1000
2019-02-21 13:29:09.311  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.poolSize, using default value: 1
2019-02-21 13:29:09.311  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.providerName, using default value: null
2019-02-21 13:29:09.311  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.providerClassName, using default value: null
2019-02-21 13:29:09.311  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.saltGeneratorClassname, using default value: org.jasypt.salt.RandomSaltGenerator
2019-02-21 13:29:09.311  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.ivGeneratorClassname, using default value: org.jasypt.salt.NoOpIVGenerator
2019-02-21 13:29:09.312  INFO 46670 --- [           main] c.u.j.encryptor.DefaultLazyEncryptor     : Encryptor config not found for property jasypt.encryptor.stringOutputType, using default value: base64
2019-02-21 13:29:09.542  INFO 46670 --- [           main] c.h.j.JasyptSampleApplication            : Started JasyptSampleApplication in 0.961 seconds (JVM running for 1.431)
2019-02-21 13:29:09.545  INFO 46670 --- [           main] c.h.j.JasyptSampleApplication            : Server password is: contactspassword

Process finished with exit code 0
```

#### Jasypt Notes

1. Download Jasypt from [here.](http://www.jasypt.org/)
2. Unzip jasypt
3. Go to jasypt folder
4. Encrypt password : 
    ```
    ./encrypt.sh input="contactspassword" password="supersecretz" algorithm="PBEWithMD5AndDES" verbose=false 
    ```
5. Decrypt password:
    ```
    ./decrypt.sh input="AFPeflMce+M0dol2e6J5QjbCbdHdk94RrMA6vGTKxHI=" password="supersecretz" algorithm="PBEWithMD5AndDES" verbose=true
    ```
    
    Result :
    ```
    ----ENVIRONMENT-----------------
    
    Runtime: Oracle Corporation Java HotSpot(TM) 64-Bit Server VM 25.202-b03 
    
    
    
    ----ARGUMENTS-------------------
    
    verbose: true
    algorithm: PBEWithMD5AndDES
    input: AFPeflMce+M0dol2e6J5QjbCbdHdk94RrMA6vGTKxHI=
    password: supersecretz
    
    
    
    ----OUTPUT----------------------
    
    contactspassword

    ```