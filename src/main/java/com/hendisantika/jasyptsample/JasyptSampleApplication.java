package com.hendisantika.jasyptsample;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
public class JasyptSampleApplication implements CommandLineRunner {

    private static Logger logger = LogManager.getLogger(JasyptSampleApplication.class);

    @Value("${server.password}")
    String serverPassword;

    public static void main(String[] args) {
        SpringApplication.run(JasyptSampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Server password is: " + serverPassword);
    }

}
